require 'sinatra'
require 'json'

require_relative 'source/PipelineStatusDetails.rb'
require_relative 'source/PublishArtifactFinder.rb'
require_relative 'source/DashBoardDataAggregator.rb'
require_relative 'source/PipelineStatusFinder.rb'
require_relative 'source/PipelineStatusDetailsList.rb'
require_relative 'source/PipelineListFetcher.rb'
require_relative 'source/PipelineGroupDetailsFinder.rb'
require_relative 'source/StatisticsCalculator'
require_relative 'source/ConfigurationReader'

get '/configuration' do
  pipelineListFetcher = PipelineListFetcher.new
  list = pipelineListFetcher.fetch
  erb :configuration, :locals => {:pipelines_list => list} 
end


get '/get_pipeline_list' do
  reader = ConfigurationReader.new
  return reader.get_pipeline_groups.to_json
end

get '/selected_pipelines' do
  pipelineListFetcher = PipelineListFetcher.new
  list = pipelineListFetcher.fetch
  erb :configuration, :locals => {:pipelines_list => list} 
end

get '/pipeline_group_statuses' do
  pipeline_status_finder = PipelineStatusFinder.new
  pipelines_statuses = pipeline_status_finder.find
  return pipelines_statuses.to_json
end

get '/build_number/:branch' do
  puts params[:branch]
  branch = params[:branch]
  finder = PublishArtifactFinder.new
  return finder.find(branch)
end

get '/get_publish_artifact_numbers' do
  finder = PublishArtifactFinder.new
  return finder.find.to_json
end

get '/get_run_details' do
  calculator = StatisticsCalculator.new
  return calculator.calculate.to_json
end

get '/' do
      erb :dashboard
end

get '/graph' do
      erb :statistics
end
