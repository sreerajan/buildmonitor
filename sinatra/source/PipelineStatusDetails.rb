class PipelineStatusDetails
  	attr_accessor :name, :current_status
  	def initialize(name, current_status)
	
		@status_to_class_map = { "pending" => "progress-warning",
	                        "passed" => "progress-success",
	                        "failed" => "progress-danger"
	         			}
    	@name = name
    	@current_status = current_status
  	end
  	def class_mappping
  		return @status_to_class_map[@current_status]
	end
end