require 'nokogiri'
require_relative 'GoServerProxy'

$go_Server_url = "http://tlgoserver01.tw.testttl.com:8153"
class DataAccessor
  def initialize
    @proxy = GoServerProxy.new
  end
  def get_pipeline_details_from_cctray
    pipeline_list = @proxy.get("#{$go_Server_url}/go/cctray.xml")
    return pipeline_list == "" ? Nokogiri::XML(File.open("backup/cctray.xml")) : Nokogiri::XML(pipeline_list) 
  end

  def get_pipelines_from_go_api
    pipeline_list = @proxy.get("#{$go_Server_url}/go/api/pipelines.xml")
    return pipeline_list == "" ? Nokogiri::XML(File.open("backup/pipelines.xml")) : Nokogiri::XML(pipeline_list) 
  end

  def get_publish_artifact_stage_from_go_api(branch)
    pipeline_list_from_go_api = get_pipelines_from_go_api
    publish_artifact_stage_url = pipeline_list_from_go_api.xpath('//pipeline')
          .select{|p| p.attr('href').include? "PublishArtifacts"+"-"+branch}.first
          .attr('href')
    pipeline_list = @proxy.get("#{publish_artifact_stage_url}")
    return pipeline_list == "" ? Nokogiri::XML(File.open("backup/publish_artifacts.xml")) : Nokogiri::XML(pipeline_list) 
  end

  def get(url)
    content = @proxy.get("#{url}")
    return content == ""  ? Nokogiri::XML(File.open("backup/default_stage_pa.xml")) : Nokogiri::XML(content) 
  end

  def get_pipeline_groups
    pipeline_list = @proxy.get("#{$go_Server_url}/go/api/admin/config/current.xml")
    return pipeline_list == "" ? Nokogiri::XML(File.open("backup/config.xml")) : Nokogiri::XML(pipeline_list) 
  end

  def get_stage_details(stage)
    pipeline_list_from_go_api =  Nokogiri::XML(File.open("backup/pipelines.xml"))
    #get_pipelines_from_go_api
    stage_url = pipeline_list_from_go_api.xpath('//pipeline')
          .select{|p| p.attr('href').include? "#{stage}"}.first
          .attr('href')
    # pipeline_list = @proxy.get("#{stage_url}")
    
    # puts "Writing backup/#{stage}.xml"
    # File.open("backup/#{stage}.xml","w"){|f| f.write(pipeline_list)}
    # puts "Done Writing backup/#{stage}.xml"
    return Nokogiri::XML(File.open("backup/#{stage}.xml")) 
    # return pipeline_list == "" ? Nokogiri::XML(File.open("backup/#{stage}.xml")) : Nokogiri::XML(pipeline_list)
  end

end
