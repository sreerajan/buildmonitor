require_relative 'DataAccessor'

class PipelineListFetcher
  def fetch
    pipeline_names = []
    dataAccessor = DataAccessor.new
    pipeline_list = dataAccessor.get_pipelines_from_go_api
    pipeline_list.xpath('//pipeline').each do |p|
      url = p.attr('href').split('/')
      pipeline_names.push(url[url.length-2])
    end
    return pipeline_names
  end
end