class DashBoardDataAggregator
  def initialize(pipelineStatusFinder)
    @pipelineStatusFinder = pipelineStatusFinder
  end
  def start(branch,components)
    pipeline_statuses = @pipelineStatusFinder.Find(branch,components)
    return pipeline_statuses
  end
end
