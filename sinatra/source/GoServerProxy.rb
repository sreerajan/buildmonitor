require 'uri'
require 'net/http'

class GoServerProxy
	def initialize
	    @username  = ENV['GO_USERNAME']
	    @password  = ENV['GO_PASSWORD']
  	end
	def get(url)

	  uri = URI.parse(url);
	  http = Net::HTTP.new(uri.host, uri.port);
	  request = Net::HTTP::Get.new(uri.request_uri) ;
	  request.basic_auth(@username, @password);
	  http.open_timeout = 10;
	  http.request(request).body;
	 
		rescue
	  		""
	end
end