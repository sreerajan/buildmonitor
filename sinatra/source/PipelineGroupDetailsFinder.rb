require_relative 'DataAccessor'

class PipelineGroupDetailsFinder
  def find
    dataAccessor = DataAccessor.new
    pipeline_groups = dataAccessor.get_pipeline_groups

    groups = []
    pipeline_groups.css('pipelines').each do |group| 
        group_name = group.attr('group')        
        group_details = {:name => group_name, :pipelines => []}
        group.css('pipeline').each do |p| 
          group_details[:pipelines].push(p.attr('name')) if p.attr('name') != nil
      end
      groups.push(group_details)
    end
    return groups
  end
end