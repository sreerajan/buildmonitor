require_relative 'DataAccessor'
require_relative 'ConfigurationReader'
require_relative 'PipelineStatusDetails'
require 'json'

class PipelineStatusFinder
  def initialize
    @status_to_class_map = { "pending" => "progress-warning",
                          "passed" => "progress-success",
                          "failed" => "progress-danger"
                }
  end
  def find
    result = []
    config_reader = ConfigurationReader.new
    pipeline_group_details = config_reader.get_pipeline_groups
    dataAccessor = DataAccessor.new
    pipeline_details_list = dataAccessor.get_pipeline_details_from_cctray
    
    pipeline_group_details.each do |group|
      group_detail = {:name => group[:name],:pipeline_statuses => []}
      group[:components].each do |component|
        current_status =  has_building_stage_in(pipeline_details_list,component) ? "pending" : "sleeping"
        previous_status = has_failure_in_previous_build(pipeline_details_list,component) ? "failed" : "passed"
        current_status = current_status == "sleeping" ? previous_status : current_status
        group_detail[:pipeline_statuses].push(
          {
              :name => component,
              :current_status => current_status,
              :class_mapping => @status_to_class_map[current_status]
          })
      end
      result.push(group_detail)
    end

    return result
  end

  def has_building_stage_in(stage_list, pipeline_name)
    stage_list.xpath('//Project')
            .select{|pipeline| (pipeline.attr('name').include? pipeline_name) and 
                      (pipeline.attr('activity') == 'Building')}.length > 0
  end

  def has_failure_in_previous_build(stage_list,pipeline_name)
    stage_list.xpath('//Project').select{|pipeline| (pipeline.attr('name').include? pipeline_name) and 
                              (pipeline.attr('lastBuildStatus') == "Failure")}.length > 0
  end
end

# finder = PipelineStatusFinder.new
# puts finder.find