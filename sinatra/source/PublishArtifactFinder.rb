require_relative 'DataAccessor'
require_relative 'ConfigurationReader'

class PublishArtifactFinder
  def find
    result = []
    dataAccessor = DataAccessor.new
    configurationReader = ConfigurationReader.new

    branches = configurationReader.get_publish_artifact_branches
    branches.each do |b|
      publish_artifacts_stage_details = dataAccessor.get_publish_artifact_stage_from_go_api(b)
      stage_details_url = publish_artifacts_stage_details.css("entry")
                                     .find{|f| f.css("title").find {|f| f.children.to_s.include? "Passed"} != nil}
                                     .css("link")
                                     .find{|d| d.attr("rel") == "alternate"}.to_a[1][1]
      publish_artifacts_details = dataAccessor.get(stage_details_url)
      result.push(
        {
          :branch => b ,
          :build_number => publish_artifacts_details.css("stage pipeline").attr("label").value
        }
      )
    end
    return result
  end
end