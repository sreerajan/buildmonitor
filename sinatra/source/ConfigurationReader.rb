#TODO Replace curl with something else : Curb, open, httparty.
require 'nokogiri'

class ConfigurationReader
  
  def get_pipeline_groups
    details = []
    config = Nokogiri::XML(File.open("config/configuration.xml"))
    config.css('Group').each do |group|  
      group_detail = {:name => group.attr('name'),:components => []}
      group.css('Component').each do |component| 
        group_detail[:components].push(component.text)
      end
      details.push(group_detail)
    end
    return details
  end

  def get_pipelines
    details = []
    config = Nokogiri::XML(File.open("config/configuration.xml"))
    stages = {:components => []}
    config.css('Stage').each do |stage|  
        stages[:components].push(stage.text)
      end
    return stages
  end

  def get_publish_artifact_branches
    result = []
    config = Nokogiri::XML(File.open("config/configuration.xml"))
    config.css('PublishArtifacts branch').each do |b| 
      result.push(b.text)
    end
    return result
  end

end

# reader = ConfigurationReader.new
# puts reader.get_pipeline_groups
