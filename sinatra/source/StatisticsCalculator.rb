require_relative 'DataAccessor'
require_relative 'ConfigurationReader'
require 'json'

class StatisticsCalculator
  def calculate
    result = []
    dataAccessor = DataAccessor.new
    config_reader = ConfigurationReader.new
    
    pipelines = config_reader.get_pipelines
  
      pipelines[:components].each do |component|
        stage_details = dataAccessor.get_stage_details(component)
        run_details   = get_yesterdays_run_details(stage_details)
        run_details[:name] = component
        result.push(run_details)
      end
    return result
  end

  def get_yesterdays_run_details(stage_details)
    yesterdays_run_details = []
    yesterday = (Time.now.to_s.split(' ')[0].split('-')[2].to_i - 7) #2013-10-17 18:29:00 +0100 - how Time.now looks!
    stage_details.css('entry').each do
          |e| yesterdays_run_details.push({:status => e.css('title').text, :date => e.css('updated').text}) 
          if e.css('updated').text.split('T')[0].split('-')[2].to_i >= yesterday   
      end
      result = {
              :name => "",
              :succes_count => yesterdays_run_details.count{|b| b[:status].include? "Passed"},
              :failure_count => yesterdays_run_details.count{|b| b[:status].include? "Failed"},
              :total_run_count => yesterdays_run_details.length
            }
      return result            
    end
  end
  
end

# finder = StatisticsCalculator.new
# puts finder.calculate