function set_pipeline_status() {
  $.ajax({
    url: 'http://localhost:4567/pipeline_group_statuses', dataType : 'json',  
    success: function(data) {
      $.each(data, function(index, val) {
        $.each(val.pipeline_statuses, function(index, value) {
          $("#"+value.name).removeClass('failed');
          $("#"+value.name).removeClass('passed');
          $("#"+value.name).addClass(value.current_status);
        });
      });
    },
    complete: function() {
      setTimeout(set_pipeline_status, 5000);
    }
  });
};

function setup_publish_artifact_numbers() {
  $.ajax({
    url: 'http://localhost:4567/get_publish_artifact_numbers',dataType : 'json',  
    success: function(data) {
        $.each(data, function(index, value) {
          $('#'+value.branch+'-build_number').remove();
          var span = $('<span id="'+value.branch+'-build_number" class="pipeline_container"></span>');
          var div = $('<div id="'+value.build_number.split('.').join("")+'"><div class="caption_pipeline">Latest Build : '+value.branch+'<br>'+value.build_number+'</div></div>');
          span.append(div);
          $( "#pipeline_container").append(span);
          $( "#"+value.build_number.split('.').join("")).progressbar({
                value: false
          });
          $( "#"+value.build_number.split('.').join("")).addClass('pipeline');
      });
    },
    complete: function() {
      setTimeout(setup_publish_artifact_numbers, 60000);
    }
  });
};

function setup_run_details() {
  $.ajax({
    url: 'http://localhost:4567/get_run_details',dataType : 'json',  
    success: function(data) {
        $('#run_details').remove();
        var span = $('<span id=run_details" style="float:left;"></span>');
        var div = $('<div>Strength</div>')
        var list = $('<ul></ul>')          
        $.each(data, function(index, value) {
          pass_percent = value.succes_count !== 0 ? (value.succes_count/value.total_run_count)*100 : 0;
          fail_percent = value.failure_count !== 0 ? (value.failure_count/value.total_run_count)*100 : 0;
          var innerDiv = $('<div  id="' + value.name + '-run" class="progress" style="width:250px;">'+value.name+'</div>');

          innerDiv.append('<div id="' + value.name + '-run-component" class="run" style="width: '+pass_percent+'%; font-size:25px;padding-top:10px; font-size:25px;padding-top:10px; background-color:#5eb95e;"></div>');          
          innerDiv.append('<div id="' + value.name + '-run-component" class="run" style="width: '+fail_percent+'%; font-size:25px;padding-top:10px; font-size:25px;padding-top:10px; background-color:#dd514c;"></div>');          
          list.append(innerDiv);
          div.append(list);
          span.append(div);

          $('#pipeline_container').append(span);
      
      });
    },
    complete: function() {
      setTimeout(setup_publish_artifact_numbers, 60000);
    }
  });
};

function setup_progress_bar() {
  $.ajax({
    url: 'http://localhost:4567/get_pipeline_list',dataType : 'json',  
    success: function(data) {
        $.each(data, function(index, value) {
          $('#'+value.name).remove();
          var span = $('<span id="'+value.name+'" class="pipeline_container"></span>');
          $.each(value.components, function(index, val) {

             var div = $('<div id="'+val+'"><div class="caption_pipeline">'+val+'</div></div>');
             span.append(div);
             $( "#pipeline_container").append(span);
             $( "#"+val).progressbar({
                value: false
            });
             $( "#"+val).addClass('pipeline');
             
          });
      });
    },
    complete: function() {
      set_pipeline_status();
    }
  });
};

(function start(){
  setup_progress_bar();
  // setup_pipeline_list();
  setup_publish_artifact_numbers();
  // setup_run_details();
})();
